var oils = {
    "type": "FeatureCollection",
    "features": [{
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "Song Hong",
            "category": "default",
            "density": 1167,
            "level": 1,
            "parentId": null,
            "Id": 1
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.935425, 20.684184],
                    [106.479492, 19.906219],
                    [106.710205, 18.667063],
                    [107.259521, 17.701595],
                    [107.935181, 17.193275],
                    [108.061523, 18.224134],
                    [108.050537, 19.26448],
                    [107.902222, 19.880392],
                    [106.935425, 20.684184]
                ]
            ]
        }
    }]
}
