var rectangles = {
    "type": "FeatureCollection",
    "features": [{
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "106",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 11,

        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.726684, 20.499064],
                    [106.726684, 20.210656],
                    [107.358398, 20.210656],
                    [107.358398, 20.499064],
                    [106.726684, 20.499064]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "107",
            "category": "default",
            "density": 104.05,
            "level": 2,
            "parentId": 1,
            "Id": 12,

        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.721191, 20.210656],
                    [106.721191, 19.921713],
                    [107.358398, 19.921713],
                    [107.358398, 20.210656],
                    [106.721191, 20.210656]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "108",
            "category": "default",
            "density": 204.05,
            "level": 2,
            "parentId": 1,
            "Id": 13,

        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.721191, 19.921713],
                    [106.721191, 19.575318],
                    [107.358398, 19.575318],
                    [107.358398, 19.921713],
                    [106.721191, 19.921713]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "109",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 14,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.221313, 19.575318],
                    [106.721191, 19.575318],
                    [106.721191, 19.921713],
                    [106.221313, 19.921713],
                    [106.221313, 19.575318]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "110",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 15,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.715698, 19.575318],
                    [106.715698, 19.119219],
                    [107.358398, 19.119219],
                    [107.358398, 19.575318],
                    [106.715698, 19.575318]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "111",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 16,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.221313, 19.119219],
                    [106.715698, 19.119219],
                    [106.715698, 19.575318],
                    [106.221313, 19.575318],
                    [106.221313, 19.119219]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "112",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 17,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.2323, 18.656654],
                    [106.715698, 18.656654],
                    [106.715698, 19.114029],
                    [106.2323, 19.114029],
                    [106.2323, 18.656654]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "113",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 18,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.715698, 19.119219],
                    [106.715698, 18.656654],
                    [107.358398, 18.656654],
                    [107.358398, 19.119219],
                    [106.715698, 19.119219]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "114",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 19,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.226807, 18.32324],
                    [106.715698, 18.32324],
                    [106.715698, 18.65145],
                    [106.226807, 18.65145],
                    [106.226807, 18.32324]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "115",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 20,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.715698, 18.656654],
                    [106.715698, 18.32324],
                    [107.358398, 18.32324],
                    [107.358398, 18.656654],
                    [106.715698, 18.656654]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "116",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 21,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.721191, 20.210656],
                    [106.435547, 20.215811],
                    [106.221313, 19.921713],
                    [106.721191, 19.921713],
                    [106.721191, 20.210656]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "117",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 22,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.226807, 18.32324],
                    [106.567383, 17.957832],
                    [107.160645, 17.936929],
                    [107.149658, 18.32324],
                    [106.226807, 18.32324]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "118",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 23,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.160645, 17.936929],
                    [108.149414, 17.936929],
                    [108.149414, 18.32324],
                    [107.160645, 18.32324],
                    [107.160645, 17.936929]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "119",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 24,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 18.333669],
                    [108.149414, 18.333669],
                    [108.149414, 18.656654],
                    [107.358398, 18.656654],
                    [107.358398, 18.333669]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "120",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 25,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 18.656654],
                    [108.149414, 18.656654],
                    [108.149414, 19.119219],
                    [107.358398, 19.119219],
                    [107.358398, 18.656654]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "121",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 26,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 19.12441],
                    [108.138428, 19.12441],
                    [108.138428, 19.575318],
                    [107.358398, 19.575318],
                    [107.358398, 19.12441]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "122",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 27,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 19.590844],
                    [108.149414, 19.590844],
                    [108.149414, 19.921713],
                    [107.358398, 19.921713],
                    [107.358398, 19.590844]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "123",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 28,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 19.921713],
                    [108.149414, 19.921713],
                    [108.149414, 20.210656],
                    [107.358398, 20.210656],
                    [107.358398, 19.921713]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "124",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 29,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.358398, 20.220966],
                    [108.138428, 20.220966],
                    [108.138428, 20.499064],
                    [107.358398, 20.499064],
                    [107.358398, 20.220966]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "125",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 30,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [108.435059, 16.045813],
                    [109.35791, 16.045813],
                    [109.35791, 16.530898],
                    [108.435059, 16.530898],
                    [108.435059, 16.045813]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "126",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 31,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 16.530898],
                    [108.435059, 16.530898],
                    [108.435059, 16.941215],
                    [107.600098, 16.941215],
                    [107.600098, 16.530898]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "127",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 32,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 16.941215],
                    [108.446045, 16.941215],
                    [108.446045, 17.497389],
                    [107.600098, 17.497389],
                    [107.600098, 16.941215]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "128",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 33,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 17.497389],
                    [107.600098, 17.936929],
                    [106.567383, 17.957832],
                    [106.820068, 17.486911],
                    [107.600098, 17.497389]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "129",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 34,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 17.497389],
                    [108.435059, 17.497389],
                    [108.435059, 17.936929],
                    [107.600098, 17.936929],
                    [107.600098, 17.497389]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "130",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 35,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [108.435059, 17.936929],
                    [108.435059, 17.497389],
                    [109.35791, 17.497389],
                    [109.35791, 17.936929],
                    [108.435059, 17.936929]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "131",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 36,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [108.446045, 17.497389],
                    [108.446045, 16.941215],
                    [109.35791, 16.941215],
                    [109.35791, 17.497389],
                    [108.446045, 17.497389]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "132",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 37,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [108.435059, 16.530898],
                    [109.35791, 16.530898],
                    [109.35791, 16.941215],
                    [108.435059, 16.941215],
                    [108.435059, 16.530898]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "133",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 38,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.369385, 20.499064],
                    [108.138428, 20.499064],
                    [108.138428, 20.95118],
                    [107.369385, 20.95118],
                    [107.369385, 20.499064]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Rectangle",
            "name": "134",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 39,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.721191, 20.488773],
                    [107.369385, 20.488773],
                    [107.369385, 20.95118],
                    [106.721191, 20.95118],
                    [106.721191, 20.488773]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "135",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 40,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 16.530898],
                    [108.127441, 16.045813],
                    [108.435059, 16.045813],
                    [108.435059, 16.530898],
                    [107.600098, 16.530898]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "136",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 41,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [107.600098, 16.941215],
                    [107.171631, 16.941215],
                    [107.600098, 16.530898],
                    [107.600098, 16.941215]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "137",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 42,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [106.820068, 17.486911],
                    [107.171631, 16.941215],
                    [107.600098, 16.941215],
                    [107.600098, 17.497389],
                    [106.820068, 17.486911]
                ]
            ]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Polygon",
            "name": "138",
            "category": "default",
            "density": 94.05,
            "level": 2,
            "parentId": 1,
            "Id": 43,
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [108.149414, 18.333669],
                    [108.149414, 18.32324],
                    [108.149414, 18.32324],
                    [108.149414, 17.936929],
                    [109.35791, 17.936929],
                    [108.841553, 18.32324],
                    [108.149414, 18.333669]
                ]
            ]
        }
    }]
}