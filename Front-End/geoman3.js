var markers = {
    "type": "FeatureCollection",
    "features": [{
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "HR-1X",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 111,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.039795, 20.293113]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "HR-1X-ST3",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 112,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.501221, 20.220966]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "HR-2X",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 113,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.270508, 19.911384]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "YT-2X",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 114,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.490234, 19.993998]
        }   
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "HR-1X-ST4",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 115,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.078247, 20.045611]
        }
    }, {
        "type": "Feature",
        "properties": {
            "shape": "Marker",
            "name": "YT-1X",
            "category": "default",
            "level": 3,
            "parentId": 11,
            "Id": 116,
        },
        "geometry": {
            "type": "Point",
            "coordinates": [107.314453, 20.390974]
        }
    }]
}